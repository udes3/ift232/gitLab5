package labo5.followers;

/*
 * Journal syst�me. Affiche dans la console le message en question.
 * Utilisez-le pour voir si les messages sont produits comme pr�vu
 * par les vedettes.
 */
public class MessageLog {
	
	public void update(String message){
		
		System.out.println(message);
		
	}

}
